/************************************************************************************
 * drivers/mtd/gd25.c
 * Driver for SPI-based GD25x16, x32, and x64 and GD25q16, q32, q64, and q128 FLASH
 *
 *   Copyright (C) 2012-2013, 2017 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/signal.h>
#include <nuttx/fs/ioctl.h>
#include <nuttx/spi/spi.h>
#include <nuttx/mtd/mtd.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
/* Configuration ********************************************************************/
/* Per the data sheet, the GD25 parts can be driven with either SPI mode 0 (CPOL=0
 * and CPHA=0) or mode 3 (CPOL=1 and CPHA=1). But I have heard that other devices
 * can operate in mode 0 or 1.  So you may need to specify CONFIG_GD25_SPIMODE to
 * select the best mode for your device.  If CONFIG_GD25_SPIMODE is not defined,
 * mode 0 will be used.
 */

#define CONFIG_GD25_SPIMODE SPIDEV_MODE0

/* SPI Frequency.  May be up to 25MHz. */

#define CONFIG_GD25_SPIFREQUENCY 20000000

/* GD25 Instructions *****************************************************************/
/*      Command                    Value      Description                           */
/*                                                                                  */
#define GD25_WREN 0x06     /* Write enable                          */
#define GD25_WRDI 0x04     /* Write Disable                         */
#define GD25_RDSR 0x05     /* Read status register                  */
#define GD25_WRSR 0x01     /* Write Status Register                 */
#define GD25_RDDATA 0x03   /* Read data bytes                       */
#define GD25_FRD 0x0b      /* Higher speed read                     */
#define GD25_FRDD 0x3b     /* Fast read, dual output                */
#define GD25_PP 0x02       /* Program page                          */
#define GD25_BE 0xd8       /* Block Erase (64KB)                    */
#define GD25_SE 0x20       /* Sector erase (4KB)                    */
#define GD25_CE 0xc7       /* Chip erase                            */
#define GD25_PD 0xb9       /* Power down                            */
#define GD25_PURDID 0xab   /* Release PD, Device ID                 */
#define GD25_RDMFID 0x90   /* Read Manufacturer / Device            */
#define GD25_JEDEC_ID 0x9f /* JEDEC ID read                         */

/* GD25 Registers ********************************************************************/
/* Read ID (RDID) register values */

#define GD25_MANUFACTURER 0xef /* GigaDevice Serial Flash */
#define GD25X16_DEVID 0x14     /* GD25X16 device ID (0xab, 0x90) */
#define GD25X32_DEVID 0x15     /* GD25X16 device ID (0xab, 0x90) */
#define GD25X64_DEVID 0x16     /* GD25X16 device ID (0xab, 0x90) */
#define GD25X128_DEVID 0x17    /* GD25X16 device ID (0xab, 0x90) */

/* JEDEC Read ID register values */

#define GD25_JEDEC_MANUFACTURER 0xC8 /* SST manufacturer ID */
#define GD25_JEDEC_MEMORY_TYPE 0x40  /* GD25X memory type */

#define GD25_JEDEC_CAPACITY_8MBIT 0x14   /* 256x4096  = 8Mbit memory capacity */
#define GD25_JEDEC_CAPACITY_16MBIT 0x15  /* 512x4096  = 16Mbit memory capacity */
#define GD25_JEDEC_CAPACITY_32MBIT 0x16  /* 1024x4096 = 32Mbit memory capacity */
#define GD25_JEDEC_CAPACITY_64MBIT 0x17  /* 2048x4096 = 64Mbit memory capacity */
#define GD25_JEDEC_CAPACITY_128MBIT 0x18 /* 4096x4096 = 128Mbit memory capacity */

#define NSECTORS_8MBIT 256    /* 256 sectors x 4096 bytes/sector = 1Mb */
#define NSECTORS_16MBIT 512   /* 512 sectors x 4096 bytes/sector = 2Mb */
#define NSECTORS_32MBIT 1024  /* 1024 sectors x 4096 bytes/sector = 4Mb */
#define NSECTORS_64MBIT 2048  /* 2048 sectors x 4096 bytes/sector = 8Mb */
#define NSECTORS_128MBIT 4096 /* 4096 sectors x 4096 bytes/sector = 16Mb */

/* Status register bit definitions */

#define GD25_SR_BUSY (1 << 0) /* Bit 0: Write in progress */
#define GD25_SR_WEL (1 << 1)  /* Bit 1: Write enable latch bit */
#define GD25_SR_BP_SHIFT (2)  /* Bits 2-5: Block protect bits */
#define GD25_SR_BP_MASK (15 << GD25_SR_BP_SHIFT)
#define GD25X16_SR_BP_NONE (0 << GD25_SR_BP_SHIFT)       /* Unprotected */
#define GD25X16_SR_BP_UPPER32nd (1 << GD25_SR_BP_SHIFT)  /* Upper 32nd */
#define GD25X16_SR_BP_UPPER16th (2 << GD25_SR_BP_SHIFT)  /* Upper 16th */
#define GD25X16_SR_BP_UPPER8th (3 << GD25_SR_BP_SHIFT)   /* Upper 8th */
#define GD25X16_SR_BP_UPPERQTR (4 << GD25_SR_BP_SHIFT)   /* Upper quarter */
#define GD25X16_SR_BP_UPPERHALF (5 << GD25_SR_BP_SHIFT)  /* Upper half */
#define GD25X16_SR_BP_ALL (6 << GD25_SR_BP_SHIFT)        /* All sectors */
#define GD25X16_SR_BP_LOWER32nd (9 << GD25_SR_BP_SHIFT)  /* Lower 32nd */
#define GD25X16_SR_BP_LOWER16th (10 << GD25_SR_BP_SHIFT) /* Lower 16th */
#define GD25X16_SR_BP_LOWER8th (11 << GD25_SR_BP_SHIFT)  /* Lower 8th */
#define GD25X16_SR_BP_LOWERQTR (12 << GD25_SR_BP_SHIFT)  /* Lower quarter */
#define GD25X16_SR_BP_LOWERHALF (13 << GD25_SR_BP_SHIFT) /* Lower half */

#define GD25X32_SR_BP_NONE (0 << GD25_SR_BP_SHIFT)       /* Unprotected */
#define GD25X32_SR_BP_UPPER64th (1 << GD25_SR_BP_SHIFT)  /* Upper 64th */
#define GD25X32_SR_BP_UPPER32nd (2 << GD25_SR_BP_SHIFT)  /* Upper 32nd */
#define GD25X32_SR_BP_UPPER16th (3 << GD25_SR_BP_SHIFT)  /* Upper 16th */
#define GD25X32_SR_BP_UPPER8th (4 << GD25_SR_BP_SHIFT)   /* Upper 8th */
#define GD25X32_SR_BP_UPPERQTR (5 << GD25_SR_BP_SHIFT)   /* Upper quarter */
#define GD25X32_SR_BP_UPPERHALF (6 << GD25_SR_BP_SHIFT)  /* Upper half */
#define GD25X32_SR_BP_ALL (7 << GD25_SR_BP_SHIFT)        /* All sectors */
#define GD25X32_SR_BP_LOWER64th (9 << GD25_SR_BP_SHIFT)  /* Lower 64th */
#define GD25X32_SR_BP_LOWER32nd (10 << GD25_SR_BP_SHIFT) /* Lower 32nd */
#define GD25X32_SR_BP_LOWER16th (11 << GD25_SR_BP_SHIFT) /* Lower 16th */
#define GD25X32_SR_BP_LOWER8th (12 << GD25_SR_BP_SHIFT)  /* Lower 8th */
#define GD25X32_SR_BP_LOWERQTR (13 << GD25_SR_BP_SHIFT)  /* Lower quarter */
#define GD25X32_SR_BP_LOWERHALF (14 << GD25_SR_BP_SHIFT) /* Lower half */

#define GD25X64_SR_BP_NONE (0 << GD25_SR_BP_SHIFT)       /* Unprotected */
#define GD25X64_SR_BP_UPPER64th (1 << GD25_SR_BP_SHIFT)  /* Upper 64th */
#define GD25X64_SR_BP_UPPER32nd (2 << GD25_SR_BP_SHIFT)  /* Upper 32nd */
#define GD25X64_SR_BP_UPPER16th (3 << GD25_SR_BP_SHIFT)  /* Upper 16th */
#define GD25X64_SR_BP_UPPER8th (4 << GD25_SR_BP_SHIFT)   /* Upper 8th */
#define GD25X64_SR_BP_UPPERQTR (5 << GD25_SR_BP_SHIFT)   /* Upper quarter */
#define GD25X64_SR_BP_UPPERHALF (6 << GD25_SR_BP_SHIFT)  /* Upper half */
#define GD25X46_SR_BP_ALL (7 << GD25_SR_BP_SHIFT)        /* All sectors */
#define GD25X64_SR_BP_LOWER64th (9 << GD25_SR_BP_SHIFT)  /* Lower 64th */
#define GD25X64_SR_BP_LOWER32nd (10 << GD25_SR_BP_SHIFT) /* Lower 32nd */
#define GD25X64_SR_BP_LOWER16th (11 << GD25_SR_BP_SHIFT) /* Lower 16th */
#define GD25X64_SR_BP_LOWER8th (12 << GD25_SR_BP_SHIFT)  /* Lower 8th */
#define GD25X64_SR_BP_LOWERQTR (13 << GD25_SR_BP_SHIFT)  /* Lower quarter */
#define GD25X64_SR_BP_LOWERHALF (14 << GD25_SR_BP_SHIFT) /* Lower half */
                                                         /* Bit 6: Reserved */
#define GD25_SR_SRP (1 << 7)                             /* Bit 7: Status register write protect */

#define GD25_DUMMY 0xa5

/* Chip Geometries ******************************************************************/
/* All members of the family support uniform 4K-byte sectors and 256 byte pages */

#define GD25_SECTOR_SHIFT 12       /* Sector size 1 << 12 = 4Kb */
#define GD25_SECTOR_SIZE (1 << 12) /* Sector size 1 << 12 = 4Kb */
#define GD25_PAGE_SHIFT 8          /* Sector size 1 << 8 = 256b */
#define GD25_PAGE_SIZE (1 << 8)    /* Sector size 1 << 8 = 256b */

#define GD25_SECTOR512_SHIFT 9       /* Sector size 1 << 9 = 512 bytes */
#define GD25_SECTOR512_SIZE (1 << 9) /* Sector size 1 << 9 = 512 bytes */

#define GD25_ERASED_STATE 0xff /* State of FLASH when erased */

struct gd25_dev_s
{
  struct mtd_dev_s mtd;  /* MTD interface */
  struct spi_dev_s *spi; /* Saved SPI interface instance */
  uint16_t nsectors;     /* Number of erase sectors */
  uint32_t dev_no;
};

//Private function declaration
static inline void gd25_lock(struct spi_dev_s *spi);
static inline void gd25_unlock(struct spi_dev_s *spi);
static uint8_t gd25_waitwritecomplete(struct gd25_dev_s *priv);
static inline int gd25_chiperase(struct gd25_dev_s *priv);
static int gd25_erase(struct mtd_dev_s *dev, off_t startblock, size_t nblocks);
static ssize_t gd25_bread(struct mtd_dev_s *dev, off_t startblock, size_t nblocks,
                          uint8_t *buffer);
static ssize_t gd25_bwrite(struct mtd_dev_s *dev, off_t startblock, size_t nblocks,
                           const uint8_t *buffer);
static void gd25_wren(struct gd25_dev_s *priv);
static void gd25_readbytes(struct gd25_dev_s *priv, uint32_t offset, size_t nbytes, uint8_t *buffer);
static void gd25_writeblock(struct gd25_dev_s *priv, uint32_t offset, uint8_t *buffer);
static void gd25_eraseblock(struct gd25_dev_s *priv, uint32_t offset);
static int gd25_ioctl(struct mtd_dev_s *dev, int cmd, unsigned long arg);

static void gd25_writeblock(struct gd25_dev_s *priv, uint32_t offset, uint8_t *buffer)
{
  printf("Write block offset %d\n", offset);
  for (int i = 0; i < 16; i++)
  {
    gd25_waitwritecomplete(priv);
    gd25_wren(priv);
    usleep(100);
    SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), true);
    SPI_SEND(priv->spi, GD25_PP);
    (void)SPI_SEND(priv->spi, (offset >> 16) & 0xff);
    (void)SPI_SEND(priv->spi, (offset >> 8) & 0xff);
    (void)SPI_SEND(priv->spi, offset & 0xff);
    for (int j = 0; j < GD25_PAGE_SIZE; j++)
    {
      (void)SPI_SEND(priv->spi, buffer[i * GD25_PAGE_SIZE + j]);
    }
    SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), false);
  }
}

static void gd25_eraseblock(struct gd25_dev_s *priv, uint32_t offset)
{
  gd25_waitwritecomplete(priv);
  gd25_wren(priv);
  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), true);
  SPI_SEND(priv->spi, GD25_SE);
  (void)SPI_SEND(priv->spi, (offset >> 16) & 0xff);
  (void)SPI_SEND(priv->spi, (offset >> 8) & 0xff);
  (void)SPI_SEND(priv->spi, offset & 0xff);
  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), false);
}

static inline void gd25_lock(struct spi_dev_s *spi)
{
  (void)SPI_LOCK(spi, true);
  SPI_SETMODE(spi, CONFIG_GD25_SPIMODE);
  SPI_SETBITS(spi, 8);
  (void)SPI_HWFEATURES(spi, 0);
  (void)SPI_SETFREQUENCY(spi, CONFIG_GD25_SPIFREQUENCY);
}

static inline void gd25_unlock(struct spi_dev_s *spi)
{
  (void)SPI_LOCK(spi, false);
}

static uint8_t gd25_waitwritecomplete(struct gd25_dev_s *priv)
{
  uint8_t status;

  /* Loop as long as the memory is busy with a write cycle. Device sets BUSY
   * flag to a 1 state whhen previous write or erase command is still executing
   * and during this time, device will ignore further instructions except for
   * "Read Status Register" and "Erase/Program Suspend" instructions.
   */

  do
  {
    /* Select this FLASH part */

    SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), true);

    /* Send "Read Status Register (RDSR)" command */

    (void)SPI_SEND(priv->spi, GD25_RDSR);

    /* Send a dummy byte to generate the clock needed to shift out the status */

    status = SPI_SEND(priv->spi, GD25_DUMMY);

    /* Deselect the FLASH */

    SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), false);

    usleep(1000);
  } while ((status & GD25_SR_BUSY) != 0);

  return status;
}

static inline int gd25_chiperase(struct gd25_dev_s *priv)
{
  finfo("priv: %p\n", priv);

  /* Wait for any preceding write or erase operation to complete. */

  (void)gd25_waitwritecomplete(priv);

  /* Send write enable instruction */

  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), true);

  /* Send "Write Enable (WREN)" command */

  (void)SPI_SEND(priv->spi, GD25_WREN);

  /* Deselect the FLASH */

  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), false);

  /* Select this FLASH part */

  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), true);

  /* Send the "Chip Erase (CE)" instruction */

  (void)SPI_SEND(priv->spi, GD25_CE);

  /* Deselect the FLASH */

  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), false);
  return OK;
}

static int gd25_erase(struct mtd_dev_s *dev, off_t startblock, size_t nblocks)
{
  struct gd25_dev_s *priv = (struct gd25_dev_s *)dev;
  gd25_lock(priv->spi);

  gd25_waitwritecomplete(priv);

  for (int i = 0; i < nblocks; i++)
  {
    gd25_eraseblock(priv, (startblock + i) * GD25_SECTOR_SIZE);
  }

  gd25_unlock(priv->spi);
  return OK;
}

static ssize_t gd25_bread(struct mtd_dev_s *dev, off_t startblock, size_t nblocks,
                          uint8_t *buffer)
{
  // printf("bread startblock %d,nblocks %d\n", startblock, nblocks);
  struct gd25_dev_s *priv = (struct gd25_dev_s *)dev;
  gd25_lock(priv->spi);
  gd25_waitwritecomplete(priv);
  gd25_readbytes(priv, startblock * GD25_SECTOR_SIZE, nblocks * GD25_SECTOR_SIZE, buffer);
  gd25_unlock(priv->spi);
  return nblocks;
}

static ssize_t gd25_bwrite(struct mtd_dev_s *dev, off_t startblock, size_t nblocks,
                           const uint8_t *buffer)
{
  // printf("bwrite startblock %d,nblocks %d\n", startblock, nblocks);
  struct gd25_dev_s *priv = (struct gd25_dev_s *)dev;
  gd25_lock(priv->spi);
  for (int i = 0; i < nblocks; i++)
  {
    uint32_t start = (startblock + i) * GD25_SECTOR_SIZE;
    gd25_eraseblock(priv, start);
    gd25_writeblock(priv, start, (uint8_t *)buffer + i * GD25_SECTOR_SIZE);
  }
  gd25_unlock(priv->spi);
  return nblocks;
}

static int gd25_ioctl(struct mtd_dev_s *dev, int cmd, unsigned long arg)
{
  struct gd25_dev_s *priv = (struct gd25_dev_s *)dev;
  int ret = OK;

  switch (cmd)
  {
  case MTDIOC_GEOMETRY:
  {
    struct mtd_geometry_s *geo = (struct mtd_geometry_s *)((uintptr_t)arg);
    if (geo)
    {
      geo->blocksize = GD25_SECTOR_SIZE;
      geo->erasesize = GD25_SECTOR_SIZE;
      geo->neraseblocks = priv->nsectors;
      finfo("blocksize: %d erasesize: %d neraseblocks: %d\n",
            geo->blocksize, geo->erasesize, geo->neraseblocks);
    }
  }
  break;

  case MTDIOC_BULKERASE:
  {
    gd25_lock(priv->spi);
    gd25_chiperase(priv);
    gd25_unlock(priv->spi);
  }
  break;

  default:
    ret = -ENOTTY;
    break;
  }
  return ret;
}

/* Some devices may support byte oriented reads (optional).  Most MTD devices
   * are inherently block oriented so byte-oriented writing is not supported. It
   * is recommended that low-level drivers not support read() if it requires
   * buffering.
   */

static void gd25_wren(struct gd25_dev_s *priv)
{
  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), true);
  (void)SPI_SEND(priv->spi, GD25_WREN);
  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), false);
}

static void gd25_readbytes(struct gd25_dev_s *priv, uint32_t offset, size_t nbytes, uint8_t *buffer)
{
  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), true);
  (void)SPI_SEND(priv->spi, GD25_FRD);
  (void)SPI_SEND(priv->spi, (offset >> 16) & 0xff);
  (void)SPI_SEND(priv->spi, (offset >> 8) & 0xff);
  (void)SPI_SEND(priv->spi, offset & 0xff);
  (void)SPI_SEND(priv->spi, GD25_DUMMY);

  for (int i = 0; i < nbytes; i++)
  {
    buffer[i] = SPI_SEND(priv->spi, GD25_DUMMY);
  }

  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), false);
}

static ssize_t gd25_read(struct mtd_dev_s *dev, off_t offset, size_t nbytes,
                         uint8_t *buffer)
{
  struct gd25_dev_s *priv = (struct gd25_dev_s *)dev;
  gd25_lock(priv->spi);
  gd25_waitwritecomplete(priv);
  gd25_readbytes(priv, offset, nbytes, buffer);
  gd25_unlock(priv->spi);
  return nbytes;
}

#ifdef CONFIG_MTD_BYTE_WRITE
static ssize_t gd25_write(struct mtd_dev_s *dev, off_t offset, size_t nbytes,
                          const uint8_t *buffer)
{
  return -EACCES;
}
#endif

/* Support other, less frequently used commands:
   *  - MTDIOC_GEOMETRY:  Get MTD geometry
   *  - MTDIOC_XIPBASE:   Convert block to physical address for eXecute-In-Place
   *  - MTDIOC_BULKERASE: Erase the entire device
   * (see include/nuttx/fs/ioctl.h)
   */

static inline int gd25_readid(struct gd25_dev_s *priv)
{
  uint16_t manufacturer;
  uint16_t memory;
  uint16_t capacity;

  finfo("priv: %p\n", priv);

  /* Lock and configure the SPI bus */

  gd25_lock(priv->spi);

  /* Wait for any preceding write or erase operation to complete. */

  (void)gd25_waitwritecomplete(priv);

  /* Select this FLASH part. */

  SPI_SELECT(priv->spi, SPIDEV_FLASH(priv->dev_no), true);

  /* Send the "Read ID (RDID)" command and read the first three ID bytes */

  (void)SPI_SEND(priv->spi, GD25_JEDEC_ID);
  manufacturer = SPI_SEND(priv->spi, GD25_DUMMY);
  memory = SPI_SEND(priv->spi, GD25_DUMMY);
  capacity = SPI_SEND(priv->spi, GD25_DUMMY);

  /* Deselect the FLASH and unlock the bus */

  SPI_SELECT(priv->spi, SPIDEV_FLASH(0), false);
  gd25_unlock(priv->spi);

  finfo("manufacturer: %02x memory: %02x capacity: %02x\n",
        manufacturer, memory, capacity);

  /* Check for a valid manufacturer and memory type */

  if (manufacturer == GD25_JEDEC_MANUFACTURER &&
      (memory == GD25_JEDEC_MEMORY_TYPE))
  {
    /* Okay.. is it a FLASH capacity that we understand? If so, save
       * the FLASH capacity.
       */

    /* 8M-bit / 1M-byte
       *
       * W25Q80BV
       */

    if (capacity == GD25_JEDEC_CAPACITY_8MBIT)
    {
      priv->nsectors = NSECTORS_8MBIT;
    }

    /* 16M-bit / 2M-byte (2,097,152)
       *
       * W24X16, W25Q16BV, W25Q16CL, W25Q16CV, W25Q16DW
       */

    else if (capacity == GD25_JEDEC_CAPACITY_16MBIT)
    {
      priv->nsectors = NSECTORS_16MBIT;
    }

    /* 32M-bit / M-byte (4,194,304)
       *
       * W25X32, W25Q32BV, W25Q32DW
       */

    else if (capacity == GD25_JEDEC_CAPACITY_32MBIT)
    {
      priv->nsectors = NSECTORS_32MBIT;
    }

    /* 64M-bit / 8M-byte (8,388,608)
       *
       * W25X64,  W25Q64BV, W25Q64CV, W25Q64DW
       */

    else if (capacity == GD25_JEDEC_CAPACITY_64MBIT)
    {
      priv->nsectors = NSECTORS_64MBIT;
    }

    /* 128M-bit / 16M-byte (16,777,216)
       *
       * W25Q128BV
       */

    else if (capacity == GD25_JEDEC_CAPACITY_128MBIT)
    {
      priv->nsectors = NSECTORS_128MBIT;
    }
    else
    {
      /* Nope.. we don't understand this capacity. */

      return -ENODEV;
    }

    return OK;
  }

  /* We don't understand the manufacturer or the memory type */

  return -ENODEV;
}

struct mtd_dev_s *gd25_initialize(struct spi_dev_s *spi, uint32_t dev_no)
{
  struct gd25_dev_s *priv;
  int ret;

  finfo("spi: %p\n", spi);

  /* Allocate a state structure (we allocate the structure instead of using
   * a fixed, static allocation so that we can handle multiple FLASH devices.
   * The current implementation would handle only one FLASH part per SPI
   * device (only because of the SPIDEV_FLASH(0) definition) and so would have
   * to be extended to handle multiple FLASH parts on the same SPI bus.
   */

  priv = (struct gd25_dev_s *)kmm_zalloc(sizeof(struct gd25_dev_s));
  if (priv)
  {
    /* Initialize the allocated structure (unsupported methods were
       * nullified by kmm_zalloc).
       */

    priv->mtd.erase = gd25_erase;
    priv->mtd.bread = gd25_bread;
    priv->mtd.bwrite = gd25_bwrite;
    priv->mtd.read = gd25_read;
    priv->mtd.ioctl = gd25_ioctl;
#if defined(CONFIG_MTD_BYTE_WRITE)
    priv->mtd.write = gd25_write;
#endif
    priv->spi = spi;
    priv->dev_no = dev_no;
    /* Deselect the FLASH */

    SPI_SELECT(spi, SPIDEV_FLASH(priv->dev_no), false);

    /* Identify the FLASH chip and get its capacity */

    ret = gd25_readid(priv);
    if (ret != OK)
    {
      /* Unrecognized! Discard all of that work we just did and return NULL */

      ferr("ERROR: Unrecognized\n");
      kmm_free(priv);
      return NULL;
    }
    return (struct mtd_dev_s *)priv;
  }

    /* Register the MTD with the procfs system if enabled */

#ifdef CONFIG_MTD_REGISTRATION
  mtd_register(&priv->mtd, "gd25");
#endif

  /* Return the implementation-specific state structure as the MTD device */

  finfo("Return %p\n", priv);
  return (struct mtd_dev_s *)priv;
}
