/************************************************************************************
 * configs/viewtool-stm32f107/src/stm32_boot.c
 *
 *   Copyright (C) 2013 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/
// #include <nuttx/compiler.h>
#include <stdio.h>
#include <fcntl.h>
#include "../bsp/include/nuttx/analog/adc.h"
#include <nuttx/analog/ioctl.h>
#include <sys/ioctl.h>
#include <nuttx/mtd/mtdh>
#include <arch/board/board.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <errno.h>

#include "stm32.h"
#include "stm32_gpio.h"
#include "stm32_adc.h"
#include "stm32_spi.h"

/************************************************************************************
 * Public Functions
 ************************************************************************************/
static const uint32_t g_adc_pinlist[] = {GPIO_ADC1_IN0, GPIO_ADC1_IN1, GPIO_ADC1_IN2, GPIO_ADC1_IN3};
static const uint8_t g_adc_chnlist[] = {0, 1, 2, 3};

#define CFG_FLASH_CS (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_CLEAR | GPIO_PORTA | GPIO_PIN4)

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry
 *point
 *   is called early in the intitialization -- after all memory has been
 *configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/
void board_app_initialize(void)
{
}

void stm32_boardinitialize(void)
{
    for (int i = 0; i < 4; i++)
    {
        stm32_configgpio(g_adc_pinlist[i]);
    }
    stm32_configgpio(CFG_FLASH_CS);
    stm32_gpiowrite(CFG_FLASH_CS, true);
}
extern struct mtd_dev_s *gd25_initialize(FAR struct spi_dev_s *spi, uint32_t dev_no);

void board_initialize(void)
{
    //initialize adc device
    struct adc_dev_s *padc = stm32_adcinitialize(1, g_adc_chnlist, 4);
    adc_register("/dev/adc", padc);
    //initialize spi & w25q128
    struct spi_dev_s *pspi = stm32_spibus_initialize(1);
    struct mtd_dev_s *pmtd_flash = gd25_initialize(pspi, 0);

    if (pmtd_flash)
    {
        ftl_initialize(0, pmtd_flash);
    }
    // if (pmtd_flash)
    // {
    //     //initialize SMART file system
    //     smart_initialize(0, pmtd_flash, 0);
    //     if (mount("/dev/smart0", "/mnt/flash", "smartfs", 0, NULL) < 0)
    //     {
    //         //if mount failed,format the file system
    //         mksmartfs("/dev/smart0", 256);
    //         //mount flash to specific path
    //         if (mount("/dev/smart0", "/mnt/flash", "smartfs", 0, NULL) == 0)
    //         {
    //         }
    //     }

    //     //try open emulated fat entity
    //     int fd = open("/mnt/flash/emu", O_RDWR);
    //     if (fd < 0)
    //     { //open failed ,try to create the file
    //         fd = open("/mnt/flash/emu", O_RDWR | O_CREAT);
    //     }

    //     if (fd > 0)
    //     {
    //         //open file emu succeed
    //         struct stat sb;
    //         stat("/mnt/flash/emu", &sb);
    //         //write file to 512KB
    //         if (sb.st_size != 1024 * 512)
    //         {
    //             uint8_t dummy[512] = {0};
    //             for (int i = 0; i < 1024; i++)
    //             {
    //                 write(fd, dummy, 512);
    //             }
    //         }
    //         close(fd);
    //         //try to map the file to a device
    //         pmtd_flash = filemtd_initialize("/mnt/flash/emu", 0, 512, 4096);
    //         if (pmtd_flash)
    //         {
    //             //mount the device
    //             ftl_initialize(0, pmtd_flash);
    //             //mount the file system
    //             if (mount("/dev/mtdblock0", "/mnt/storage", "vfat", 0, 0) < 0)
    //             {
    //                 //mount failed,format the file system
    //                 struct fat_format_s format = FAT_FORMAT_INITIALIZER;
    //                 mkfatfs("/dev/mtdblock0", &format);
    //                 //mount the file system to specific path
    //                 mount("/dev/mtdblock0", "/mnt/storage", "vfat", 0, 0);
    //             }
    //         }
    //     }
    // }
}
extern int nsh_main(void);

uint8_t buffer[4096];

int weak_function main(void)
{
    int fd = open("/dev/mtdblock0", O_RDWR);
    if (fd > 0)
    {
        int err = 0;
        for (int i = 0; i < sizeof(buffer); i++)
        {
            buffer[i] = i % 255;
        }
        // printf("\n");
        lseek(fd, 4096, SEEK_SET);
        printf("\nWrited %d\n", write(fd, buffer, sizeof(buffer)));
        fsync(fd);
        lseek(fd, 4096, SEEK_SET);
        printf("Read   %d\n", read(fd, buffer, sizeof(buffer)));
        for (int i = 0; i < sizeof(buffer); i++)
        {
            if (buffer[i] != i % 255)
            {
                err++;
                // printf("Error,exp %x,val %x\n", i % 255, buffer[i]);
            }
            else
            {
                printf("Index %d\n", i);
            }
        }
        printf("Err %d\n", err);
    }
    nsh_main();
    return 0;
}

void stm32_usbsuspend(FAR struct usbdev_s *dev, bool resume)
{
}

uint8_t stm32_spi1status(FAR struct spi_dev_s *dev, uint32_t devid)
{
    return SPI_STATUS_PRESENT;
}

uint8_t stm32_spi4status(FAR struct spi_dev_s *dev, uint32_t devid)
{
    return SPI_STATUS_PRESENT;
}

void stm32_spi1select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
    if (selected)
    {
        selected = false;
    }
    else
    {
        selected = true;
    }

    switch (devid)
    {
    case SPIDEV_FLASH(0):
        stm32_gpiowrite(CFG_FLASH_CS, selected);
        break;

    default:
        break;
    }
}

void stm32_spi4select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
}
