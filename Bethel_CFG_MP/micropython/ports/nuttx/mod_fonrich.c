#include "py/runtime.h"
#include "py/mphal.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


extern const mp_obj_type_t mod_csv_type;

static const mp_rom_map_elem_t mod_fonrich_globals_table[] = {
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_fonrich)},
    {MP_ROM_QSTR(MP_QSTR_csv), MP_ROM_PTR(&mod_csv_type)},
};

static MP_DEFINE_CONST_DICT(mod_fonrich_globals, mod_fonrich_globals_table);

const mp_obj_module_t mod_fonrich = {
    .base = {&mp_type_module},
    .globals = (mp_obj_dict_t *)&mod_fonrich_globals,
};
