// qstrs specific to this port
Q(fonrich)
Q(csv)
Q(write)
Q(close)
//system required
Q(array)
Q(ustruct)
Q(struct)
Q(calcsize)
Q(pack)
Q(pack_into)
Q(sizeof)
Q(addressof)
Q(bytes_at)
Q(bytearray_at)
Q(NATIVE)
Q(LITTLE_ENDIAN)
Q(BIG_ENDIAN)
Q(VOID)
Q(UINT8)
Q(UINT16)
Q(INT16)
Q(UINT32)
Q(FLOAT64)
Q(PTR)

Q(INT32)
Q(unpack)
Q(unpack_from)
Q(bytearray)
Q(filter)
Q(memoryview)
Q(enumerate)
Q(doc)
Q(property)
Q(deleter)
Q(getter)
Q(setter)
Q(reversed)
Q(__reversed__)
Q(frozenset)
Q(union)
Q(symmetric_difference)
Q(issuperset)
Q(issubset)
Q(isdisjoint)
Q(intersection)
Q(difference)
Q(set)
Q(symmetric_difference_update)
Q(difference_update)
Q(intersection_update)
Q(discard)
Q(add)
Q(slice)
Q(min)
Q(max)
Q(default)
Q(ucollections)
Q(namedtuple)
Q(___aiter__)
Q(___anext__)
Q(___aenter__)
Q(___aexit__)
Q(native)
Q(viper)
Q(uint)
Q(ptr)
Q(ptr8)
Q(ptr16)
Q(ptr32)
Q(None)
Q(label)
Q(data)
Q(align)
Q(asm_thumb)
Q(ldr)
Q(ldrb)
Q(ldrh)
Q(strb)
Q(vcmp)
Q(vsqrt)
Q(vneg)
Q(strh)
Q(vcvt_s32_f32)
Q(vcvt_f32_s32)
Q(vmrs)
Q(vmov)
Q(vldr)
Q(vstr)
Q(pop)
Q(wfi)
Q(b)
Q(bl)
Q(bx)
Q(cpsid)
Q(push)
Q(mov)
Q(clz)
Q(rbit)
Q(mrs)
Q(and_)
Q(cmp)
Q(sub)
Q(movw)
Q(movt)
Q(movwt)
Q(ldrex)
Q(lsl)
Q(lsr)
Q(asr)
Q(sdiv)
Q(udiv)
Q(strex)
Q(nop)
Q(cpsie)
Q(ViperTypeError)
Q(__doc__)
Q(uzlib)
Q(read)
Q(uctypes)
Q(ARRAY)
Q(DecompIO)
Q(uio)
Q(decompress)
Q(readline)
Q(ujson)
Q(dumps)
Q(seek)
Q(flush)
Q(getvalue)
Q(StringIO)
Q(loads)
Q(load)
Q(BF_POS)
Q(BFINT16)
Q(BF_LEN)
Q(BFINT32)
Q(INT64)
Q(UINT64)
Q(INT8)
Q(BFUINT32)
Q(BFUINT16)
Q(BFINT8)
Q(BFUINT8)
Q(readinto)
Q(FLOAT32)
