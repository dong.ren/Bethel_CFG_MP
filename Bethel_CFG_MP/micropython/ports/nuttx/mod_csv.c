#include "py/runtime.h"
#include "py/mphal.h"
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

const mp_obj_type_t mod_csv_type;

typedef struct
{
    mp_obj_base_t base;
    int fd;
} mod_csv_obj_t;

static void mod_csv_obj_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind)
{
    mod_csv_obj_t *self = self_in;
    mp_printf(print, "CSV(%d)", self->fd);
}
//WARNING : Memroy leak
static mp_obj_t mod_csv_obj_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *args)
{
    // check arguments
    static mp_obj_base_t common_base = {&mod_csv_type};
    mp_arg_check_num(n_args, n_kw, 1, 1, false);

    size_t len = 0;
    char fs[32];
    const char *path = mp_obj_str_get_data(args[0], &len);
    int fsize = snprintf(fs, 32, "/mnt/flash/%s", path);

    if (fsize < 0)
    {
        nlr_raise(mp_obj_new_exception_msg_varg(&mp_type_OSError, "Open file %s failed!", path));
    }
    if (fsize <= len)
    {
        nlr_raise(mp_obj_new_exception_msg_varg(&mp_type_OSError, "File name %s is too long!", path));
    }
    fsize = open(fs, O_RDWR | O_CREAT);

    if (fsize < 0)
    {
        nlr_raise(mp_obj_new_exception_msg_varg(&mp_type_OSError, "Open file %s failed!", path));
    }

    // pyb_led_obj[0].fd = fsize;

    mod_csv_obj_t *obj = malloc(sizeof(mod_csv_obj_t));
    if (obj == NULL)
    {
        nlr_raise(mp_obj_new_exception_msg_varg(&mp_type_OSError, "Allocl memory failed for CSV object $s!", path));
    }

    obj->base = common_base;
    // uint32_t *buf = (uint32_t *)&obj->base;
    // *buf = *(uint32_t *)&mod_csv_type;
    obj->fd = fsize;
    return obj;
    // return (mp_obj_t)pyb_led_obj;
}

#define CSV_CHECK_TYPE(src, obj) (0 == strcmp(src, obj))

static void mod_csv_write_base_type_to_fs(int fd, mp_obj_t arg)
{
    const char *type_str = mp_obj_get_type_str(arg);

    if (CSV_CHECK_TYPE(type_str, "array"))
    {
        size_t lens = 0;
        mp_obj_t *p = NULL;
        mp_obj_get_array(arg, &lens, &p);
        dprintf(fd, "[");
        for (int j = 0; j < lens; j++)
        {
            mod_csv_write_base_type_to_fs(fd, p[j]);
            if (j != lens - 1)
            {
                dprintf(fd, ",");
            }
        }
        dprintf(fd, "]");
    }
    else if (CSV_CHECK_TYPE(type_str, "list"))
    {
        size_t lens = 0;
        mp_obj_t *p = NULL;
        mp_obj_list_get(arg, &lens, &p);
        dprintf(fd, "[");
        for (int j = 0; j < lens; j++)
        {
            mod_csv_write_base_type_to_fs(fd, p[j]);
            if (j != lens - 1)
            {
                dprintf(fd, ",");
            }
        }
        dprintf(fd, "]");
    }
    else if (CSV_CHECK_TYPE(type_str, "int"))
    {
        dprintf(fd, "%d", mp_obj_get_int(arg));
    }
    else if (CSV_CHECK_TYPE(type_str, "float"))
    {
        dprintf(fd, "%f", mp_obj_get_float(arg));
    }
    else if (CSV_CHECK_TYPE(type_str, "str"))
    {
        dprintf(fd, "\"%s\"", mp_obj_str_get_str(arg));
    }
    else
    {
        nlr_raise(mp_obj_new_exception_msg_varg(&mp_type_Exception, "Unsupport arguments type: %s!", type_str));
    }
}

#undef CSV_CHECK_TYPE

static mp_obj_t mod_csv_write(size_t n_args, const mp_obj_t *args)
{
    if (n_args <= 1)
    {
        nlr_raise(mp_obj_new_exception_msg_varg(&mp_type_Exception, "Invalid arguments nums: %d!", n_args - 1));
    }
    mod_csv_obj_t *self = (mod_csv_obj_t *)args[0];
    for (int index = 1; index < n_args; index++)
    {

        mod_csv_write_base_type_to_fs(self->fd, args[index]);

        if (index == n_args - 1)
        {
            dprintf(self->fd, "\n");
        }
        else
        {
            dprintf(self->fd, ",");
        }
    }
    fsync(self->fd);
    return mp_const_none;
}

static mp_obj_t mod_csv_close(mp_obj_t self_in)
{

    return mp_const_none;
}

static MP_DEFINE_CONST_FUN_OBJ_VAR(mod_csv_write_obj, 1, mod_csv_write);
static MP_DEFINE_CONST_FUN_OBJ_1(mod_csv_close_obj, mod_csv_close);

static const mp_rom_map_elem_t mod_csv_locals_dict_table[] = {
    {MP_ROM_QSTR(MP_QSTR_write), MP_ROM_PTR(&mod_csv_write_obj)},
    {MP_ROM_QSTR(MP_QSTR_close), MP_ROM_PTR(&mod_csv_close_obj)},
};

static MP_DEFINE_CONST_DICT(mod_csv_locals_dict, mod_csv_locals_dict_table);

const mp_obj_type_t mod_csv_type = {
    {&mp_type_type},
    .name = MP_QSTR_csv,
    .print = mod_csv_obj_print,
    .make_new = mod_csv_obj_make_new,
    .locals_dict = (mp_obj_dict_t *)&mod_csv_locals_dict,
};
