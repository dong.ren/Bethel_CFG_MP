#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>

extern int nsh_main(void);
extern void pyexec_file(const char *src);

int main()
{
    //dummy usage to linking
    pyexec_file(NULL);
    nsh_main();
    return 0;
}
